package com.zaloni.hackathon2015.codingmaniacs.resources;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.regex.Pattern;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.zaloni.hackathon2015.codingmaniacs.common.Constants;
import com.zaloni.hackathon2015.codingmaniacs.config.ApplicationConfiguration;
import com.zaloni.hackathon2015.codingmaniacs.dto.GenericTaskStatus;

/**
 * @author Bishwajit
 */

@Path("/agent")
@Produces(MediaType.APPLICATION_JSON)
public class AgentResource {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AgentResource.class);
	private static final String DISTRO_FILE_NAME = "flume-ng.tar.gz";
	private static final String AGENT_SERVICE_FILE_NAME = "flume-service.sh";
	private static final String JOLOKIA_AGENT_FILE = "jolokia-jvm-1.2.3-agent.jar";

	private ApplicationConfiguration appConfig;
	private String deploymentDirectory;

	public AgentResource(ApplicationConfiguration applicationConfiguration) {
		this.appConfig = applicationConfiguration;
		this.deploymentDirectory = MessageFormat.format(
				"/home/{0}/data_pipeline", appConfig.getLoginUser());
	}

	@POST
	@Timed
	@Path("/deploy/{ipAddress}")
	public GenericTaskStatus deployAgent(
			@NotNull @PathParam("ipAddress") String ipAddresses) {
		try {
			LOGGER.info("Deploying agent on {}", ipAddresses);
			for (String ipAddress : ipAddresses.split(Pattern.quote(","))) {
				if (ipAddress.trim().isEmpty()) {
					continue;
				}
				Session session = getSession(appConfig, ipAddress, 22);
				session.connect();
				final ChannelSftp sftp = (ChannelSftp) session
						.openChannel("sftp");
				sftp.connect();
				uploadFiles(deploymentDirectory,
						appConfig.getApplicationHome(), sftp);
				sftp.disconnect();
				LOGGER.info("File uploading complete.");

				final String createPermission = MessageFormat.format(
						"chmod 755 {0}/flume-service.sh", deploymentDirectory);
				int statusCode = executeCommand(session, createPermission);
				if (statusCode != 0) {
					throw new Exception("Command '" + createPermission
							+ "' has non zero return code.");
				}

				final String createDirCommand = MessageFormat.format(
						"mkdir -p {0}/flume-dist", deploymentDirectory);
				statusCode = executeCommand(session, createDirCommand);
				if (statusCode != 0) {
					throw new Exception("Command '" + createDirCommand
							+ "' has non zero return code.");
				}

				final String extractCommand = MessageFormat
						.format("tar zxf {0}/{1} -C {0}/flume-dist/ --strip-components=1",
								deploymentDirectory, DISTRO_FILE_NAME);
				LOGGER.info("Untaring files:" + extractCommand);
				statusCode = executeCommand(session, extractCommand);
				if (statusCode != 0) {
					throw new Exception("Command '" + extractCommand
							+ "' has non zero return code.");
				}

				final String removeCommand = MessageFormat.format("rm {0}/{1}",
						deploymentDirectory, DISTRO_FILE_NAME);
				statusCode = executeCommand(session, removeCommand);
				if (statusCode != 0) {
					throw new Exception("Command '" + removeCommand
							+ "' has non zero return code.");
				}

				session.disconnect();
				LOGGER.info("Files extracted.");
			}

		} catch (Exception e) {
			LOGGER.error("Error while deploying agent on {}.", ipAddresses, e);
			return new GenericTaskStatus(false, e.getMessage());
		}
		return new GenericTaskStatus(true, "Agent deployment Successful.");
	}

	@POST
	@Timed
	@Path("/remove/{ipAddress}")
	public GenericTaskStatus removeAgent(
			@NotNull @PathParam("ipAddress") String ipAddresses) {
		try {
			LOGGER.info("Removing agent from {}.", ipAddresses);
			for (String ipAddress : ipAddresses.split(Pattern.quote(","))) {
				if (ipAddress.trim().isEmpty()) {
					continue;
				}
				Session session = getSession(appConfig, ipAddress, 22);
				session.connect();

				final String removeCommand = MessageFormat.format("rm -r {0}",
						deploymentDirectory);
				int statusCode = executeCommand(session, removeCommand);
				if (statusCode != 0) {
					throw new Exception("Command '" + removeCommand
							+ "' has non zero return code.");
				}

				session.disconnect();
				LOGGER.info("Deployment directory removed.");
			}
		} catch (Exception e) {
			LOGGER.error("Error while removing agent from {}.", ipAddresses, e);
			return new GenericTaskStatus(false, e.getMessage());
		}
		return new GenericTaskStatus(true, "Agent removal Successful.");
	}

	@POST
	@Timed
	@Path("/start/{ipAddress}")
	public GenericTaskStatus startAgent(
			@NotNull @PathParam("ipAddress") String ipAddresses) {
		try {
			LOGGER.info("Starting agent on {}.", ipAddresses);
			for (String ipAddress : ipAddresses.split(Pattern.quote(","))) {
				if (ipAddress.trim().isEmpty()) {
					continue;
				}
				Session session = getSession(appConfig, ipAddress, 22);
				session.connect();

				final String startCommand = MessageFormat.format(
						"{0}/{1} start", deploymentDirectory,
						AGENT_SERVICE_FILE_NAME);
				int statusCode = executeCommand(session, startCommand);
				if (statusCode != 0) {
					throw new Exception("Command '" + startCommand
							+ "' has non zero return code.");
				}

				session.disconnect();
				LOGGER.info("Agent started.");
			}
		} catch (Exception e) {
			LOGGER.error("Error while starting agent on {}.", ipAddresses, e);
			return new GenericTaskStatus(false, e.getMessage());
		}
		return new GenericTaskStatus(true, "Starting agent Successful.");
	}

	@POST
	@Timed
	@Path("/stop/{ipAddress}")
	public GenericTaskStatus stopAgent(
			@NotNull @PathParam("ipAddress") String ipAddresses) {
		try {
			LOGGER.info("Stopping agent on {}.", ipAddresses);
			for (String ipAddress : ipAddresses.split(Pattern.quote(","))) {
				if (ipAddress.trim().isEmpty()) {
					continue;
				}
				Session session = getSession(appConfig, ipAddress, 22);
				session.connect();

				final String stopCommand = MessageFormat.format("{0}/{1} stop",
						deploymentDirectory, AGENT_SERVICE_FILE_NAME);
				int statusCode = executeCommand(session, stopCommand);
				if (statusCode != 0) {
					throw new Exception("Command '" + stopCommand
							+ "' has non zero return code.");
				}

				session.disconnect();
				LOGGER.info("Agent stopped.");
			}
		} catch (Exception e) {
			LOGGER.error("Error while stopping agent on {}.", ipAddresses, e);
			return new GenericTaskStatus(false, e.getMessage());
		}
		return new GenericTaskStatus(true, "Stopping agent Successful.");
	}

	@POST
	@Timed
	@Path("/conf/{ipAddress}")
	public GenericTaskStatus getAgentConf(
			@NotNull @PathParam("ipAddress") String ipAddress) {
		return new GenericTaskStatus(true, "Task Successful.");
	}

	@POST
	@Timed
	@Path("/status/{ipAddress}")
	public GenericTaskStatus getAgentStatus(
			@NotNull @PathParam("ipAddress") String ipAddress) {
		try {
			LOGGER.info("Getting status of agent on {}.", ipAddress);
			Session session = getSession(appConfig, ipAddress, 22);
			session.connect();

			final ChannelSftp sftp = (ChannelSftp) session.openChannel("sftp");
			sftp.connect();

			SftpATTRS attributes = null;
			try {
				attributes = sftp.stat(deploymentDirectory);
			} catch (Exception e) {
				LOGGER.info("Deployment directory {} not found.",
						deploymentDirectory);
			}

			sftp.disconnect();
			session.disconnect();

			boolean agentIsPresent = (attributes != null && attributes.isDir());
			return new GenericTaskStatus(agentIsPresent, "Agent is "
					+ (agentIsPresent ? "" : "not ") + "deployed on "
					+ ipAddress);
		} catch (Exception e) {
			LOGGER.error("Error while getting status of agent on {}.",
					ipAddress, e);
			return new GenericTaskStatus(false, e.getMessage());
		}
	}

	@POST
	@Timed
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/configure/{ipAddress}")
	public GenericTaskStatus configureAgent(
			@NotNull @PathParam("ipAddress") String ipAddress,
			@NotNull @FormParam(value = "confText") String confText) {
		LOGGER.info(ipAddress + "," + confText);
		try {
			String[] properties = confText.split(Pattern.quote(","));
			Session session = getSession(appConfig, ipAddress, 22);
			session.connect();
			final ChannelSftp sftp = (ChannelSftp) session.openChannel("sftp");
			sftp.connect();
			StringBuilder flumeConfFileContent = new StringBuilder();
			for (String property : properties) {
				flumeConfFileContent.append(property.replace(";", "=") + "\n");
			}

			InputStream propertyStream = new ByteArrayInputStream(
					flumeConfFileContent.toString().getBytes(
							StandardCharsets.UTF_8));
			String destFlumeConf = deploymentDirectory
					+ Constants.PATH_SEPARATOR + "flume-dist/conf/flume.conf";
			LOGGER.info("Uploading flume config files:" + destFlumeConf);
			sftp.put(propertyStream, destFlumeConf);
		} catch (Exception e) {
			LOGGER.error("Error while configuring flume source and sink", e);
			return new GenericTaskStatus(false, e.getMessage());
		}
		return new GenericTaskStatus(true,
				"Source sink flume configuration Successful.");
	}

	private void uploadFiles(String deploymentDirectory,
			String applicationHome, ChannelSftp sftp) throws Exception {
		try {
			SftpATTRS attributes = sftp.stat(deploymentDirectory);
			LOGGER.trace("{} is a directory: {}.", deploymentDirectory,
					attributes.isDir());
		} catch (Exception e) {
			LOGGER.info(
					"Deployment directory {} not found. Will try to create one.",
					deploymentDirectory);
			sftp.mkdir(deploymentDirectory);
		}

		sftp.cd(deploymentDirectory);
		File fileToUpload = new File(applicationHome + Constants.PATH_SEPARATOR
				+ appConfig.getFlumeDistribution());
		LOGGER.trace("Uploading file {}.", fileToUpload.getAbsolutePath());
		sftp.put(new FileInputStream(fileToUpload), DISTRO_FILE_NAME);
		fileToUpload = new File(applicationHome + Constants.PATH_SEPARATOR
				+ AGENT_SERVICE_FILE_NAME);
		LOGGER.trace("Uploading file {}.", fileToUpload.getAbsolutePath());
		sftp.put(new FileInputStream(fileToUpload), AGENT_SERVICE_FILE_NAME);
		fileToUpload = new File(applicationHome + Constants.PATH_SEPARATOR
				+ JOLOKIA_AGENT_FILE);
		LOGGER.trace("Uploading jolokia:{}", fileToUpload);
		LOGGER.trace("Uploading file {}.", fileToUpload.getAbsolutePath());
		sftp.put(new FileInputStream(fileToUpload), JOLOKIA_AGENT_FILE);
	}

	private int executeCommand(final Session session, final String command)
			throws Exception {
		LOGGER.info("Executing command: {}.", command);
		final ChannelExec channel = (ChannelExec) session.openChannel("exec");
		channel.setCommand(command);
		channel.setPty(true);
		channel.connect();
		final InputStream input = channel.getInputStream();
		final Reader reader = new InputStreamReader(input);
		final BufferedReader buffered = new BufferedReader(reader);

		while (true) {
			final String line = buffered.readLine();
			if (line == null) {
				break;
			}
			LOGGER.info(">>> " + line);
		}
		input.close();

		int count = 50;
		final int delay = 100;
		while (true) {
			if (channel.isClosed()) {
				break;
			}
			if (count-- < 0) {
				break;
			}
			Thread.sleep(delay);
		}

		LOGGER.trace("Exec channel closed: " + channel.isClosed());
		final int status = channel.getExitStatus();
		channel.disconnect();
		return status;
	}

	private Session getSession(ApplicationConfiguration appConfig, String host,
			int port) throws JSchException {
		JSch sshClient = new JSch();
		Session session = sshClient.getSession(appConfig.getLoginUser(), host,
				port);
		session.setPassword("zaloni.12");
		/*
		 * sshClient.setKnownHosts(MessageFormat.format("/home/{0}/.ssh/known_hosts"
		 * , appConfig.getLoginUser()));
		 * sshClient.addIdentity(MessageFormat.format("/home/{0}/.ssh/id_rsa",
		 * appConfig.getLoginUser()));
		 */
		// If two machines have SSH passwordless login setup, the following line
		// is not needed:
		// session.setPassword("YOUR_PASSWORD");
		// Make it so we do not do host key checking. Enabling this would
		// require some extra code and maintenance,
		// but would increase security.
		session.setConfig("StrictHostKeyChecking", "no");
		session.setTimeout(30000);
		return session;
	}
}
