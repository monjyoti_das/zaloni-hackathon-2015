package com.zaloni.hackathon2015.codingmaniacs.util;

import org.eclipse.jetty.proxy.ProxyServlet;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;

/**
 * @author Bishwajit
 */
public class JolokiaProxy extends ProxyServlet.Transparent {
    public static final String JOLOKIA_PORT = "8778";

    @Override
    protected URI rewriteURI(HttpServletRequest request) {
        String path = request.getRequestURI();
        String stringAfterJolokia = path.substring(path.indexOf("jolokia") + "jolokia".length() + 1);
        String jolokiaIP = stringAfterJolokia.substring(0, stringAfterJolokia.indexOf('/'));
        String realPath = stringAfterJolokia.substring(stringAfterJolokia.indexOf(jolokiaIP) + jolokiaIP.length());
        String proxyTo = "http://" + jolokiaIP + ":" + JOLOKIA_PORT + "/jolokia/" + realPath;
        String query = request.getQueryString();
        if (query != null)
            proxyTo += "?" + query;
        return URI.create(proxyTo ).normalize();
    }
}
