package com.zaloni.hackathon2015.codingmaniacs.config;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;


/**
 * Configuration for the application
 *
 * @author Bishwajit
 */
public class ApplicationConfiguration extends Configuration {
    @NotEmpty
    private String loginUser = "root";

    @NotEmpty
    private String applicationHome;

    @NotEmpty
    private String flumeDistribution;


    @JsonProperty
    public String getLoginUser() {
        return loginUser;
    }

    @JsonProperty
    public void setLoginUser(String loginUser) {
        this.loginUser = loginUser;
    }

    @JsonProperty
    public String getApplicationHome() {
        return applicationHome;
    }

    @JsonProperty
    public void setApplicationHome(String applicationHome) {
        this.applicationHome = applicationHome;
    }

    @JsonProperty
    public String getFlumeDistribution() {
        return flumeDistribution;
    }

    @JsonProperty
    public void setFlumeDistribution(String flumeDistribution) {
        this.flumeDistribution = flumeDistribution;
    }

}
