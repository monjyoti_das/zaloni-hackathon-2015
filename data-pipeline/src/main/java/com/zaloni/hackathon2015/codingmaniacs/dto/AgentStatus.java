package com.zaloni.hackathon2015.codingmaniacs.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Bishwajit
 */
public class AgentStatus {
    private String status;

    public AgentStatus(String status) {
        this.status = status;
    }

    @JsonProperty
    public String getStatus() {
        return status;
    }
}
