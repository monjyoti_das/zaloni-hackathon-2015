package com.zaloni.hackathon2015.codingmaniacs.application;

import com.zaloni.hackathon2015.codingmaniacs.config.ApplicationConfiguration;
import com.zaloni.hackathon2015.codingmaniacs.resources.AgentResource;
import com.zaloni.hackathon2015.codingmaniacs.util.JolokiaProxy;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * @author Bishwajit
 */
public class DataPipelineApplication extends Application<ApplicationConfiguration> {
    public static void main(String[] args) throws Exception {
        new DataPipelineApplication().run(args);
    }

    @Override
    public String getName() {
        return "Data pipeline deployment Application";
    }

    @Override
    public void initialize(Bootstrap<ApplicationConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets/css", "/css", null, "css"));
        bootstrap.addBundle(new AssetsBundle("/assets/js", "/js", null, "js"));
        bootstrap.addBundle(new AssetsBundle("/assets/images", "/images", null, "images"));
        bootstrap.addBundle(new AssetsBundle("/assets", "/", "index.html"));
    }

    @Override
    public void run(ApplicationConfiguration applicationConfiguration, Environment environment) throws Exception {
        final AgentResource resource = new AgentResource(applicationConfiguration);
        ServletHolder servletHolder = environment.getApplicationContext().addServlet(JolokiaProxy.class, "/jolokia/*");
        servletHolder.setInitParameter("proxyTo", "");
        servletHolder.setInitParameter("prefix", "/");
        environment.jersey().setUrlPattern("/api/*");
        environment.jersey().register(resource);
    }
}
