package com.zaloni.hackathon2015.codingmaniacs.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Bishwajit
 */
public class GenericTaskStatus {
    private String message;
    private boolean success;

    public GenericTaskStatus(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    @JsonProperty
    public boolean isSuccess() {
        return success;
    }

    @JsonProperty
    public String getMessage() {
        return message;
    }
}
