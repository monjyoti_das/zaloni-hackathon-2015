package com.zaloni.hackathon2015.codingmaniacs.dto;

public class FlumeConf {
	private String ipAddress;
	private String source;
	private String sink;
	private String sourceDetails;
	private String sinkDetails;
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getSink() {
		return sink;
	}
	public void setSink(String sink) {
		this.sink = sink;
	}
	public String getSourceDetails() {
		return sourceDetails;
	}
	public void setSourceDetails(String sourceDetails) {
		this.sourceDetails = sourceDetails;
	}
	public String getSinkDetails() {
		return sinkDetails;
	}
	public void setSinkDetails(String sinkDetails) {
		this.sinkDetails = sinkDetails;
	}
	@Override
	public String toString() {
		return "FlumeConf [ipAddress=" + ipAddress + ", source=" + source
				+ ", sink=" + sink + "]";
	}
	
	

}
