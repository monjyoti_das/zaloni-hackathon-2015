/**
 * Created by Bishwajit
 */

var hostConfig = {};

function prepareconfigForHost(host){
    var agent = $("div[data-ip='" + host +"']" );
    var conf = 'FLUME_AGENT.sources;SOURCE,FLUME_AGENT.sinks;SINK,FLUME_AGENT.channels;MCHANNEL,FLUME_AGENT.channels.MCHANNEL.type;memory,FLUME_AGENT.channels.MCHANNEL.capacity;10000,FLUME_AGENT.channels.MCHANNEL.transactionCapacity;10000,FLUME_AGENT.sources.SOURCE.channels;MCHANNEL,FLUME_AGENT.sinks.SINK.channel;MCHANNEL';
    var config = hostConfig[host];
    var temp='';
    
    if(config['spool-dir']){
        temp = temp + ',' + 'FLUME_AGENT.sources.SOURCE.type;spooldir,FLUME_AGENT.sources.SOURCE.spoolDir;#SPOOL_DIR#,FLUME_AGENT.sources.SOURCE.fileHeader;false,FLUME_AGENT.sources.SOURCE.decodeErrorPolicy;IGNORE,FLUME_AGENT.sources.SOURCE.deserializer.maxLineLength;6000';
        temp = temp.replace('#SPOOL_DIR#', config['spool-dir']);
    }
    
    if(config['hdfs-sink']) {
        temp = temp + ',' + 'FLUME_AGENT.sinks.SINK.type;hdfs,FLUME_AGENT.sinks.SINK.hdfs.writeFormat;Text,FLUME_AGENT.sinks.SINK.hdfs.fileType;DataStream,FLUME_AGENT.sinks.SINK.hdfs.path;#HDFS_PATH#,FLUME_AGENT.sinks.SINK.hdfs.rollInterval;3600,FLUME_AGENT.sinks.SINK.hdfs.rollCount;0,FLUME_AGENT.sinks.SINK.hdfs.idleTimeout;600,FLUME_AGENT.sinks.SINK.hdfs.inUsePrefix;_,FLUME_AGENT.sinks.SINK.hdfs.rollSize;0,FLUME_AGENT.sinks.SINK.hdfs.batchSize;300,FLUME_AGENT.sinks.SINK.hdfs.round;true,FLUME_AGENT.sinks.SINK.hdfs.roundValue;1,FLUME_AGENT.sinks.SINK.hdfs.roundUnit;hour,FLUME_AGENT.sinks.SINK.hdfs.useLocalTimeStamp;true,FLUME_AGENT.sinks.SINK.hdfs.threadsPoolSize;30,FLUME_AGENT.sinks.SINK.hdfs.callTimeout;30000,FLUME_AGENT.sinks.SINK.serializer;text,FLUME_AGENT.sinks.SINK.serializer.appendNewline;true';
        temp = temp.replace('#HDFS_PATH#', config['hdfs-sink']);
    }
    
    if(config['avro-src-port']){
        temp = temp + ',' + 'FLUME_AGENT.sources.SOURCE.type;avro,FLUME_AGENT.sources.SOURCE.bind;0.0.0.0,FLUME_AGENT.sources.SOURCE.port;#PORT#';
        temp = temp.replace('#PORT#', config['avro-src-port']);
    }  
    
    
    if(config['avro-sink-host'] && config['avro-sink-port']){
        temp = temp + ',' + 'FLUME_AGENT.sinks.SINK.type;avro,FLUME_AGENT.sinks.SINK.hostname;#IP#,FLUME_AGENT.sinks.SINK.port;#PORT#';
        temp = temp.replace('#PORT#', config['avro-sink-port']);
        temp = temp.replace('#IP#', config['avro-sink-host']);
    }
    return conf + ',' + temp;
}

function addNodes() {
    var nodes = $('#nodeNames').val();
    nodesArray = nodes.replace(/\n/g, ",").split(',');
    console.log(nodesArray);
    for(var i=0; i<nodesArray.length; i++){
        var node = nodesArray[i];
        $('#flow-designer').append('<div class="agent" data-ip="' + node + '" data-running="false"> <div class="toolbar ui-widget-header ui-corner-all"> <button class="deploy">Deploy</button> <button class="run">Run</button> <button class="stop">Stop</button> <button class="remove">Remove</button> </div> <div class="status-container"> <img src="images/red-led.png" width="32" height="32" class="led-off"> <img src="images/green-led-on.png" width="32" height="32" class="led-on" style="display: none"> </div> </div>');
        $("div[data-ip='" + node +"']" ).draggable({ cursor: "move"});
        $("div[data-ip='" + node +"']" ).find(".deploy").button({icons: {
            primary: "ui-icon-arrowthickstop-1-n"
        }, text: false});

        $("div[data-ip='" + node +"']" ).find( ".run" ).button({icons: {
            primary: "ui-icon-play"
        }, text: false});

        $("div[data-ip='" + node +"']" ).find( ".stop" ).button({icons: {
            primary: "ui-icon-stop"
        }, text: false});

        $("div[data-ip='" + node +"']" ).find( ".remove" ).button({icons: {
            primary: "ui-icon-closethick"
        }, text: false});

        hostConfig[node] = {};
    }

    $( this ).dialog( "close" );
}

$( document ).ready(function() {

    $('#flow-designer').on('click', '.run', function() {
        var agentElement = $(this).parent().parent();
        var ip = agentElement.data("ip");
        $.ajax({
            type: 'POST',
            url: '/api/agent/start/' + ip,
            dataType: 'json',
            success: function(jsonData) {
                if(jsonData.success){
                    agentElement.find('.status-container').find('.led-on').show();
                    agentElement.find('.status-container').find('.led-off').hide();
                }
            },
            error: function() {
                alert("Could not start agent.");
            }
        });

    });

    $('#flow-designer').on('click', '.deploy', function() {
        var deploymentSuccessful = false;
        var confSuccessful = false;

        var agentElement = $(this).parent().parent();
        var ip = agentElement.data("ip");
        $.ajax({
            type: 'POST',
            url: '/api/agent/deploy/' + ip,
            dataType: 'json',
            success: function(jsonData) {
                if(jsonData.success){
                    deploymentSuccessful = true;
                }
            },
            error: function() {
                alert("Deployment unsuccessful");
            }
        }).then (function (args) { // this will run if the above .get succeeds
            var flumeConf = prepareconfigForHost(ip);

            $.ajax({
                type: 'POST',
                url: '/api/agent/configure/' + ip,
                data: {confText: flumeConf},
                success: function(jsonData) {
                    if(jsonData.success){
                        confSuccessful = true;
                    }
                },
                error: function() {
                    alert("Configuration unsuccessful");
                }
            });
        });
    });

    $('#flow-designer').on('click', '.stop', function() {
        var agentElement = $(this).parent().parent();
        var ip = agentElement.data("ip");
        $.ajax({
            type: 'POST',
            url: '/api/agent/stop/' + ip,
            dataType: 'json',
            success: function(jsonData) {
                if(jsonData.success){
                    agentElement.find('.status-container').find('.led-off').show();
                    agentElement.find('.status-container').find('.led-on').hide();
                }
            },
            error: function() {
                alert("Could not stop agent.");
            }
        });
    });

    $('#flow-designer').on('click', '.remove', function() {
        var agentElement = $(this).parent().parent();
        var ip = agentElement.data("ip");
        $.ajax({
            type: 'POST',
            url: '/api/agent/remove/' + ip,
            dataType: 'json',
            success: function(jsonData) {
                if(jsonData.success){
                    alert("Removal Successful");
                } else {
                    alert("Removal unsuccessful");
                }
            },
            error: function() {
                alert("Could not remove agent.");
            }
        });
    });

    $('#flow-designer').on('dblclick', '.flume-component', function() {
        if($(this).hasClass('spool-source')){
            spoolDialog['currentHost'] = $(this).parent().parent().data('ip');
            spoolDialog.dialog( "open" );
        } else if($(this).hasClass('avro-source')){
            spoolDialog['currentHost'] = $(this).parent().parent().data('ip');
            avroSourceDialog.dialog( "open" );
        } else if($(this).hasClass('hdfs-sink')){
            spoolDialog['currentHost'] = $(this).parent().parent().data('ip');
            hdfsSinkDialog.dialog( "open" );
        } else if($(this).hasClass('avro-sink')){
            spoolDialog['currentHost'] = $(this).parent().parent().data('ip');
            avroSinkDialog.dialog( "open" );
        }

        $('#temp-data').data('ip', $(this).parent().parent().data('ip'));
    });

    var nodeDialog = $( "#add-node-dialog-form" ).dialog({
        autoOpen: false,
        height: 300,
        width: 350,
        modal: true,
        buttons: {
            "Add Nodes": addNodes,
            "Cancel": function() {
                $( this ).dialog( "close" );
            }
        }
    });

    $('#add-node-button').click( function() {
        nodeDialog.dialog( "open" );
    });

    var spoolDialog = $( "#spool-dir-dialog-form" ).dialog({
        autoOpen: false,
        height: 160,
        width: 350,
        modal: true,
        buttons: {
            "OK": function() {
                var dir = $('#spool-directory').val();
                hostConfig[$('#temp-data').data('ip')]['spool-dir'] = dir;
                $('#spool-dir-form')[0].reset();
                $( this ).dialog( "close" );
                console.log(hostConfig);
            },
            "Cancel": function() {
                $('#spool-dir-form')[0].reset();
                $( this ).dialog( "close" );
            }
        }
    });

    var avroSourceDialog = $( "#avro-source-dialog-form" ).dialog({
        autoOpen: false,
        height: 160,
        width: 350,
        modal: true,
        buttons: {
            "OK": function() {
                var port = $('#avro-source-port').val();
                hostConfig[$('#temp-data').data('ip')]['avro-src-port'] = port;
                $('#avro-source-form')[0].reset();
                $( this ).dialog( "close" );
            },
            "Cancel": function() {
                $('#avro-source-form')[0].reset();
                $( this ).dialog( "close" );
            }
        }
    });

    var avroSinkDialog = $( "#avro-sink-dialog-form" ).dialog({
        autoOpen: false,
        height: 275,
        width: 298,
        modal: true,
        buttons: {
            "OK": function() {
                var port = $('#avro-sink-port').val();
                var host = $('#avro-sink-host').val();

                hostConfig[$('#temp-data').data('ip')]['avro-sink-host'] = host;
                hostConfig[$('#temp-data').data('ip')]['avro-sink-port'] = port;
                $('#avro-sink-form')[0].reset();
                $( this ).dialog( "close" );
            },
            "Cancel": function() {
                $('#avro-sink-form')[0].reset();
                $( this ).dialog( "close" );
            }
        }
    });

    var hdfsSinkDialog = $( "#hdfs-sink-dialog-form" ).dialog({
        autoOpen: false,
        height: 160,
        width: 350,
        modal: true,
        buttons: {
            "OK": function() {
                var path = $('#hdfs-sink-path').val();
                hostConfig[$('#temp-data').data('ip')]['hdfs-sink'] = path;
                $('#hdfs-sink-form')[0].reset();
                $( this ).dialog( "close" );

                console.log(hostConfig);
            },
            "Cancel": function() {
                $('#hdfs-sink-form')[0].reset();
                $( this ).dialog( "close" );
            }
        }
    });
});

$(function() {
    $("#flow-designer").contextmenu({
        delegate: ".agent",
        menu: [
            {title: "Add Source", children: [
                {title: "Spool Directory", cmd: "addSpoolDirectory"},
                {title: "Avro", cmd: "addAvroSource"}
            ]},
            {title: "Add Sink", children: [
                {title: "Avro", cmd: "addAvroSink"},
                {title: "HDFS", cmd: "addHDFSSink"}
            ]}
        ],
        select: function(event, ui) {
            if(ui.cmd === 'addSpoolDirectory') {
                $(ui.target.context).append('<div class="flume-component flume-source spool-source"><img src="images/directory.png" alt="Spool Directory" title="Spool Directory" /></div>');
            } else if(ui.cmd === 'addAvroSource') {
                $(ui.target.context).append('<div class="flume-component flume-source avro-source"><img src="images/avro-source.png" alt="Avro" title="Avro" /></div>');
            } else if(ui.cmd === 'addHDFSSink') {
                $(ui.target.context).append('<div class="flume-component flume-sink hdfs-sink"><img src="images/hdfs.png" alt="HDFS" title="HDFS" /></div>');
            } else if(ui.cmd === 'addAvroSink') {
                $(ui.target.context).append('<div class="flume-component flume-sink avro-sink"><img src="images/avro-sink.png" alt="Avro" title="Avro" /></div>');
            }
        }
    });
});

