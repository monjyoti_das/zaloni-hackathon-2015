#!/usr/bin/env bash

# First argument to the script is the action to be taken

function usage() {
    echo "Usage: $0 {start|stop|restart}"
    exit 1
}

if [ "$#" -lt 1 ]; then
   usage
fi

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
BASE_DIR=${SCRIPT_DIR}

# Set the Log directory here
LOG_DIR="$BASE_DIR/logs"
# Log file location
LOG_FILE="$LOG_DIR/daemon.log"

DAEMON_DATA_DIR="$BASE_DIR/.daemon_pid"
DAEMON_PID_FILE="$DAEMON_DATA_DIR/daemon.pid"


if [ ! -w "$LOG_DIR" ] ; then
  mkdir -p "$LOG_DIR"
fi

if [ ! -d "$DAEMON_DATA_DIR" ]; then
  mkdir -p "$DAEMON_DATA_DIR"
fi

export LOG_DIR=$LOG_DIR

START_COMMAND="${BASE_DIR}/flume-dist/bin/flume-ng agent -n FLUME_AGENT --conf ${BASE_DIR}/flume-dist/conf/ -f ${BASE_DIR}/flume-dist/conf/flume.conf"

echo ""
echo "Logging goes to "$LOG_FILE
# Check the first parameter
case $1 in
start)
    echo "Starting Daemon ..."
    if [ -f $DAEMON_PID_FILE ]; then
      if kill -0 `cat $DAEMON_PID_FILE` > /dev/null 2>&1; then
         echo Daemon already running as process `cat $DAEMON_PID_FILE`.
         exit 0
      fi
    fi
    nohup $START_COMMAND >$LOG_FILE 2>&1 < /dev/null &
    if [ $? -eq 0 ]
    then
      if /bin/echo -n $! > "$DAEMON_PID_FILE"
      then
        echo "Checking status of DAEMON ..."
        sleep 5
        # check if it's really started
        if kill -0 `cat $DAEMON_PID_FILE` > /dev/null 2>&1; then
          java -jar ${BASE_DIR}/jolokia-jvm-1.2.3-agent.jar --quiet start `cat $DAEMON_PID_FILE`
          echo STARTED
        else
          rm "$DAEMON_PID_FILE"
          echo "DAEMON DID NOT START. Check log for more details at $LOG_FILE"
          exit 1
        fi
      else
        echo FAILED TO WRITE PID
        exit 1
      fi
    else
      echo "DAEMON DID NOT START. Check log for more details at $LOG_FILE"
      exit 1
    fi
    ;;
stop)
    echo "Stopping DAEMON ..."
    if [ ! -f "$DAEMON_PID_FILE" ]
    then
      echo "No DAEMON to stop (could not find file $DAEMON_PID_FILE)"
    else
      kill -9 $(cat "$DAEMON_PID_FILE")
      java -jar ${BASE_DIR}/jolokia-jvm-1.2.3-agent.jar --quiet stop `cat ${DAEMON_PID_FILE}`
      rm "$DAEMON_PID_FILE"
      echo STOPPED
    fi
    exit 0
    ;;
restart)
    "$0" stop
    sleep 3
    "$0" start
    ;;
*)
    usage

esac
